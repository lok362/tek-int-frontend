
export function shortName(name: string) {
    let nuName = name.substr(0, name.length - 4);
    if (nuName.length > 8) nuName = nuName.substr(0, 8);
    return nuName + "(...)" + name.substr(name.length - 4);
}
