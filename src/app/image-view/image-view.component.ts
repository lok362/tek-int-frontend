import { Component } from "@angular/core";
import { ImageService, GalleryImage } from "../image.service";
import { trigger, transition, style, animate } from "../../../node_modules/@angular/animations";

@Component({
  selector: "app-image-view",
  templateUrl: "./image-view.component.html",
  styleUrls: ["./image-view.component.scss"],
  animations: [
    trigger("fade", [
      transition(":enter", [
        style({ opacity: 0 }),
        animate(".1s ease", style({ opacity: 1 }))
      ]),
      transition(":leave", [
        style({ opacity: 1 }),
        animate(".1s ease", style({ opacity: 0 }))
      ])
    ])
  ]
})
export class ImageViewComponent {
  image: GalleryImage;

  constructor(private readonly imageService: ImageService) {
    this.imageService.imageSelection.subscribe(img => this.image = img);
  }

  public deleteImage() {
    if (!this.image) return;
    this.imageService.deleteImage(this.image);
    this.image = undefined;
  }

  public downloadImage() {
    if (!this.image) return;
    const url = URL.createObjectURL(this.image.data, { oneTimeOnly: true });
    window.open(url, this.image.name);
  }

}
