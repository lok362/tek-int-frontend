import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { ImageCardComponent } from "./image-card/image-card.component";
import { ImageGalleryComponent } from "./image-gallery/image-gallery.component";
import { ImageService } from "./image.service";
import { ImageViewComponent } from "./image-view/image-view.component";
import { ToastComponent } from "./toast/toast.component";
import { ToastService } from "./toast/toast.service";

@NgModule({
  declarations: [
    AppComponent,
    ImageCardComponent,
    ImageGalleryComponent,
    ImageViewComponent,
    ToastComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [
    ImageService,
    ToastService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
