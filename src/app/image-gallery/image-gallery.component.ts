import { Component, Input, ViewChild, ElementRef, ChangeDetectorRef, OnInit } from "@angular/core";
import { ImageService, GalleryImage } from "../image.service";
import { trigger, transition, style, animate, state } from "@angular/animations";
import { ToastService } from "../toast/toast.service";

@Component({
  selector: "app-image-gallery",
  templateUrl: "./image-gallery.component.html",
  styleUrls: ["./image-gallery.component.scss"],
  animations: [
    trigger("item", [
      transition(":enter", [
        style({ opacity: 0 }),
        animate(".3s ease", style({ opacity: 1 }))
      ]),
      transition(":leave", [
        style({ opacity: 1 }),
        animate(".3s ease", style({ opacity: 0, transform: "scale(0.8, 0.8)" }))
      ])
    ])
  ]
})
export class ImageGalleryComponent implements OnInit {
  images: GalleryImage[] = [];

  uploadModalVisible = false;
  @ViewChild("fileSelect") fileSelect: ElementRef;

  constructor(
    private readonly imageService: ImageService
  ) {}

  public ngOnInit() {
    this.imageService.images.subscribe(images => {
      this.images = images;
    });
  }

  showUploadModal() {
    this.uploadModalVisible = true;
  }

  closeModal() {
    this.uploadModalVisible = false;
  }

  async fileSelected() {
    this.uploadModalVisible = false;
    const files = (this.fileSelect.nativeElement as HTMLInputElement).files;
    this.imageService.uploadImages(files);
  }

}
