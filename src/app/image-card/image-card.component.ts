import { Component, Input } from "@angular/core";
import { ImageService, GalleryImage } from "../image.service";

@Component({
  selector: "app-image-card",
  template: "<img [src]=\"image.data||'assets/placeholder.jpg'\" (click)=\"select()\">",
  styleUrls: ["./image-card.component.scss"]
})
export class ImageCardComponent {

  @Input() image: GalleryImage;

  constructor(private readonly imageService: ImageService) { }

  public select() {
    this.imageService.imageSelection.next(this.image);
  }

}
