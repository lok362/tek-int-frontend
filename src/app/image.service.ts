import { Injectable } from "@angular/core";
import { Subject, BehaviorSubject } from "rxjs";
import { ToastService } from "./toast/toast.service";
import { shortName } from "./util";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ImageService {

  public images = new BehaviorSubject<GalleryImage[]>([]);

  public imageSelection = new Subject<GalleryImage>();

  constructor(
    private readonly toastService: ToastService,
    private readonly http: HttpClient
  ) {
    this.getImages().then(() => {
      this.toastService.makeToast({
        message: `${this.images.value.length} images loaded.`
      });
    });
  }

  public async uploadImages(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      const imageData = await new Promise<{ name: string, data: string }>(resolve => {
        const file = files.item(i);

        if (file.type !== "image/png" && file.type !== "image/jpeg") {
          this.toastService.makeToast({
            message: `${shortName(file.name)} is not a PNG or JPG/JPEG!`
          });
          return;
        }
        if (file.size > 1000000) {
          this.toastService.makeToast({
            message: `${shortName(file.name)} is too large! (Max 1MB)`
          });
          return;
        }

        const reader = new FileReader();
        reader.onload = () => {
          resolve({
            name: file.name,
            data: reader.result as string
          });
        };
        reader.readAsDataURL(file);
      });

      const response =
      await this.http.post<{success: true, id: string }|{ success: false, reason: string }>("api/image", imageData).toPromise();

      if (response.success === true) {
        this.toastService.makeToast({
          message: `${shortName(imageData.name)} uploaded!`
        });
      } else if (response.success === false) {
        this.toastService.makeToast({
          message: `Unable to upload ${shortName(imageData.name)}. Reason: ${response.reason}`
        });
      }
    }
    await this.getImages();
  }

  public async deleteImage(image: GalleryImage) {
    const id = image.id;
    const res = await this.http.delete<{ success: true }|{ success: false, reason: string }>(`api/image/${id}`).toPromise();

    if (res.success === true) {
      this.toastService.makeToast({
        message: `${shortName(image.name)} deleted!`
      });
      await this.getImages();
    } else if (res.success === false) {
      this.toastService.makeToast({
        message: `Unable to delete ${shortName(image.name)}! Reason: ${res.reason}`
      });
    }
  }

  private async getImages() {
    this.images.next((await this.http.get<{ success: boolean, images: GalleryImage[] }>("api/image").toPromise()).images);
  }

}

export interface GalleryImage {
  id: string;
  name: string;
  data: string;
}
