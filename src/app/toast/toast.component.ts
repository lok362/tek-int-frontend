import { Component } from "@angular/core";
import { Toast, ToastService } from "./toast.service";
import { trigger, transition, style, animate } from "@angular/animations";

@Component({
  selector: "app-toast",
  templateUrl: "./toast.component.html",
  styleUrls: ["./toast.component.scss"],
  animations: [
    trigger("toast", [
      transition(":enter", [
        style({ opacity: 0, transform: "translateX(200px)" }),
        animate(".2s ease-out", style({ opacity: 1, transform: "translateX(0)" }))
      ]),
      transition(":leave", [
        style({ opacity: 1, transform: "translateX(0)", "z-index": -1000 }),
        animate(".2s ease-out", style({ opacity: 0, transform: "translateX(200px)" }))
      ])
    ])
  ]
})
export class ToastComponent {

  public toasts: Toast[];

  constructor(private readonly toastService: ToastService) {
    this.toastService.toastList.subscribe(tl => this.toasts = tl);
  }

}
