import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ToastService {

  public toastList = new BehaviorSubject<Toast[]>([]);

  public makeToast(toast: Toast) {
    if (!toast.duration) toast.duration = 2400;

    this.toastList.next([
      ...this.toastList.value,
      toast
    ]);

    setTimeout(() => {
      const list = this.toastList.value;
      const index = list.findIndex(t => t === toast);
      if (index !== -1) {
        list.splice(index, 1);
        this.toastList.next(list);
      }
    }, toast.duration);
  }

}

export interface Toast {
  message: string;
  duration?: number;
}
